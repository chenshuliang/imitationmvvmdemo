package com.csl.imitation.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.csl.imitation.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 86135
 * on 2020/12/15
 */
public abstract class BaseAdapter<T,VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    public Context mContext;
    public List<T> mList;//数据源
    public LayoutInflater inflater;

    public BaseAdapter(Context mContext) {
        this.mContext = mContext;
        this.mList=new ArrayList<>();
        inflater= (LayoutInflater) mContext.getApplicationContext().getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public VH onCreateViewHolder( ViewGroup parent, int viewType) {
        return onCreate(parent,viewType);
    }

    @Override
    public void onBindViewHolder( VH vh, int position) {
        onBindVH(vh,position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    /**
     * 创建 view Holder
     * @param parent
     * @param viewType
     * @return
     */
    public abstract VH onCreate(ViewGroup parent, int viewType);

    /**
     * 绑定 View Holder
     * @param vh
     * @param position
     */
    public abstract void onBindVH(VH vh,int position);

    /**
     * 刷新数据
     * @param data 数据源
     */
    public void refreshData(List<T> data){
        mList.clear();
        mList.addAll(data);
        notifyDataSetChanged();
    }

    /**
     * 加载更多
     * @param data 加载新的数据
     */
    public void loadMoreData(List<T> data){
        mList.addAll(data);
        notifyDataSetChanged();
    }

}
