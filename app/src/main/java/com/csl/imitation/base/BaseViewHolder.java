package com.csl.imitation.base;


import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by 86135
 * on 2020/12/15
 */
public class BaseViewHolder<B extends ViewDataBinding> extends RecyclerView.ViewHolder {
    /**
     * ViewDataBinding
     */
    private B mBinding;

    public BaseViewHolder(B biding) {
        super(biding.getRoot());
        mBinding = biding;
    }

    /**
     *
     * @return ViewDataBinding
     */
    public B getBinding() {
        return mBinding;
    }
}
