package com.csl.imitation.utils;

import android.content.Context;
import android.icu.text.UnicodeSetSpanner;
import android.view.Gravity;
import android.widget.GridView;
import android.widget.Toast;

/**
 * Created by 86135
 * on 2020/12/15
 */
public class ToastUtils {
    private static Toast toast;

    /**
     * show toast
     * @param context
     * @param msg
     */
    public static void show(Context context,String msg){
        if (toast==null){
            toast=Toast.makeText(context.getApplicationContext(),"",Toast.LENGTH_SHORT);
        }
        toast.setText(msg);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

    public static void show(Context context,int msgId){
        if (toast==null){
            toast=Toast.makeText(context.getApplicationContext(),"",Toast.LENGTH_SHORT);
        }
        toast.setText(msgId);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }
}
