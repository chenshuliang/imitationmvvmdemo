package com.csl.imitation.view;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.csl.imitation.R;

/**
 * Created by 86135
 * on 2020/12/15
 */
public class MainActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //先测试一下，看看是否已经创建成功。
    }
}
