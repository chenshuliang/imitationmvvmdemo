package com.csl.imitation.retrofitinterface;

import com.csl.imitation.bean.NewsBean;
import com.csl.imitation.constant.URLConstant;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by 86135
 * on 2020/12/15
 */
public interface RetrofitInterface {

    //获取"分类中搜索商品" 的数据
    @GET(URLConstant.URL_PATH)
    Observable<NewsBean> getNewsData();
}
