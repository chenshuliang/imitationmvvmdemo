package com.csl.imitation.bean;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

/**
 * Created by 86135
 * on 2020/12/15
 */
public class SimpleNewsBean {
    public ObservableInt color=new ObservableInt();
    public ObservableField<String> thumbnail=new ObservableField<>();
    public ObservableField<String> description=new ObservableField<>();
    public ObservableInt id=new ObservableInt();
    public ObservableField<String> name=new ObservableField<>();
    public ObservableBoolean isGood=new ObservableBoolean();//是否点赞
}
