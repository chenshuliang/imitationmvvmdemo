package com.csl.imitation;

import android.app.Application;
import android.content.Context;

/**
 * Created by 86135
 * on 2020/11/20
 */
public class MyApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
