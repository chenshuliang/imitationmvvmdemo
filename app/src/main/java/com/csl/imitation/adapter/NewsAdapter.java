package com.csl.imitation.adapter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.csl.imitation.BR;
import com.csl.imitation.R;
import com.csl.imitation.base.BaseAdapter;
import com.csl.imitation.base.BaseViewHolder;
import com.csl.imitation.bean.SimpleNewsBean;
import com.csl.imitation.utils.ToastUtils;

/**
 * Created by 86135
 * on 2020/12/15
 */
public class NewsAdapter extends BaseAdapter<SimpleNewsBean, BaseViewHolder> {


    public NewsAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public BaseViewHolder onCreate(ViewGroup parent, int viewType) {
        ViewDataBinding dataBinding= DataBindingUtil.inflate(inflater, R.layout.item_news,parent,false);
        return new BaseViewHolder(dataBinding);
    }

    @Override
    public void onBindVH(BaseViewHolder baseViewHolder, int position) {
        ViewDataBinding binding=baseViewHolder.getBinding();
        binding.setVariable(BR.simpleNewsBean,mList.get(position));
        binding.setVariable(BR.position,position);
        binding.setVariable(BR.adapter,this);
        binding.executePendingBindings();//防止闪烁
    }


    public void clickDianZan(SimpleNewsBean simpleNewsBean,int position){
        if (simpleNewsBean.isGood.get()){
            simpleNewsBean.isGood.set(false);
            ToastUtils.show(mContext,"取消点赞"+position);
        }else {
            simpleNewsBean.isGood.set(true);
            ToastUtils.show(mContext,"点赞成功"+position);
        }
    }
}
